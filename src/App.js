import React, { Component } from 'react';
import './App.css'
import {Layout, Header, Navigation, Drawer, Content } from "react-mdl";
import Routes from "./components/Routes";
import { NavLink } from "react-router-dom";


class App extends Component {
  render() {
    return (
      <div className="demo-big-content">
        <Layout>
            <Header title="Title" scroll>
                <Navigation>
                    <NavLink to="/">Home</NavLink>
                    <NavLink to="/about">About</NavLink>
                    <NavLink to="/contact">Contact</NavLink>
                </Navigation>
            </Header>
            <Drawer title="Title">
                <Navigation>
                    <NavLink to="/">Home</NavLink>
                    <NavLink to="/about">About</NavLink>
                    <NavLink to="/contact">Contact</NavLink>
                </Navigation>
            </Drawer>
            <Content>
                <div className="page-content" />
                <Routes/>
            </Content>
        </Layout>
      </div>
    );
  }
}

export default App;
